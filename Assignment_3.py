#!/usr/bin/env python
# coding: utf-8

# In[6]:


class Person:
    def __init__(self, name, designation):
        self.name = name
        self.designation = designation

    def learn(self):
        print('{} is learning'.format(self.name))

    def walk(self):
        print('{} is walking'.format(self.name))

    def eat(self):
        print('{} is eating'.format(self.name))


class Programmer(Person):
    def __init__(self,name,designation, companyName):
        super().__init__(name, designation)
        self.companyName = companyName

    def coding(self):
        print('{} is {} working in {} company'.format(self.name, self.designation, self.companyName))


class Dancer(Person):
    def __init__(self,name,designation, groupName):
        super().__init__(name, designation)
        self.groupName = groupName

    def dancing(self):
        print('{} is {} dancing in {} group'.format(self.name, self.designation, self.groupName))


class Singer(Person):
    def __init__(self,name,designation, bandName):
        super().__init__(name, designation)
        self.bandName = bandName

    def singing(self):
        print('{} is {} singing in {} brand'.format(self.name, self.designation, self.bandName))

    def playGitar(self):
        print('{} is playing gitar in {} group'.format(self.name, self.bandName))


person_A = Person('SOMA', 'SOFTWERE DEVELOPER')
person_A.learn()
person_A.walk()
person_A.eat()

programmer_1 = Programmer('RAJU','Data scientist', 'SYSTECH')
programmer_1.coding()



dancer_1 = Dancer('SANKAR','dancer', '123')
dancer_1.dancing()

singer_1 = Singer('POORNESH','singer','abc')
singer_1.singing()
singer_1.playGitar()


# In[ ]:




